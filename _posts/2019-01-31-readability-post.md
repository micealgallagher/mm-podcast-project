---
layout: post
title: "Tech Meeting Minutes"
description: "First meeting of the tech team"
date: 2018-12-31
tags: [meeting-minutes, tech]
comments: true
share: true
google-doc: "https://docs.google.com/document/d/e/2PACX-1vR2LEHiEoETjrCvPNo1vaSAbiXgOZFaaP92XyHt5uS4MoP5zQutbkoc1nXNaOoTjOvlwnMTkOyqXcTI/pub?embedded=true"
---