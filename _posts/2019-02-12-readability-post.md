---
layout: post
title: "General Meeting Minutes"
description: "Tech meeting minutes review, TOR discussion and next steps"
date: 2019-01-12
tags: [meeting-minutes]
comments: true
share: true
google-doc: "https://docs.google.com/document/d/e/2PACX-1vQnDMFtqlTgYM86z4L9M8CO03cSFFziZUSg3zFzA2-g5KSsCz4NZF0f6jQLNZV2gC4JM63fBTStldNn/pub?embedded=true"
---